//Vasilii Iurev 2032356
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0] = new Bicycle("some manufacturer", 1, 15);
        bicycles[1] = new Bicycle("good bikes", 6, 22);
        bicycles[2] = new Bicycle("cool bikes", 12, 26);
        bicycles[3] = new Bicycle("really good bikes", 18, 35);

        for (int i=0; i < bicycles.length; i++){
            System.out.println(bicycles[i]);
        }
    }
}
